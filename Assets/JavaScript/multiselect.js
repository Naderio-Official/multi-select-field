class Multiselect {

    constructor(selector) {
        this.originalSelect = this.readSelectField(selector)

        this.element = document.querySelector('.multiselect')

        this.leftItemWindow = this.element
            .querySelector('.item-window.left')

        this.rightItemWindow = this.element
            .querySelector('.item-window.right')

        this.leftItems = this.element
            .querySelector('.item-window.left > .item-window--items')
        this.rightItems = this.element
            .querySelector('.item-window.right > .item-window--items')

        this.init();
    }

    readSelectField(selector) {
        return document.querySelector(selector)
    }

    addItemToSelectList(item) {
        // move item
        this.leftItems.append(item)

        // set original option to selected
        this.originalSelect
            .querySelector(`option[value="${item.dataset.value}"]`)
            .selected = true

        // move original option to the bottom
        this.originalSelect.lastChild.after(this.originalSelect
            .querySelector(`option[value="${item.dataset.value}"]`))

    }

    removeItemFromSelectList(item) {
        this.rightItems.append(item)
        this.originalSelect
            .querySelector(`option[value="${item.dataset.value}"]`)
            .selected = false
    }

    moveItemUpInSelectList(item) {
        let previousSiblingsValue
        if (item.previousElementSibling) {
            previousSiblingsValue = item.previousElementSibling.dataset.value
            item.parentNode
                .insertBefore(item, item.previousElementSibling);
        }

        // move associated item in original select field
        this.originalSelect.querySelector(`option[value="${previousSiblingsValue}"]`)
            .before(this.originalSelect.querySelector(`option[value="${item.dataset.value}"]`))
    }

    moveItemDownInSelectList(item) {
        let nextSiblingsValue
        if (item.nextElementSibling) {
            nextSiblingsValue = item.nextElementSibling.dataset.value
            item.parentNode
                .insertBefore(item.nextElementSibling, item);
        }

        // move associated item in original select field
        this.originalSelect.querySelector(`option[value="${nextSiblingsValue}"]`)
            .after(this.originalSelect.querySelector(`option[value="${item.dataset.value}"]`))
    }

    init() {
        // this.originalSelect.style.display = 'none'

        this.populateMultiselect()

        this.updateSelectedItems()

        this.populateOriginalOptions()

        this.originalSelect.addEventListener('change', () => {
            this.updateSelectedItems();
        });

        this.element
            .querySelectorAll('.item-window > .item-window--items > .select-item > .select-item--buttons > div')
            .forEach((btn) => {
                btn.addEventListener('click', (evt) => {
                    if (btn.dataset.method === 'up') {
                        this.moveItemUpInSelectList(btn.parentNode.parentNode)
                    }
                    if (btn.dataset.method === 'down') {
                        this.moveItemDownInSelectList(btn.parentNode.parentNode)
                    }
                })
            })


        this.element
            .querySelectorAll('.item-window > .item-window--items > .select-item > .select-item--text')
            .forEach((item) => {
                item.addEventListener('click', (evt) => {
                    if (evt.composedPath().includes(this.leftItemWindow)) {
                        this.removeItemFromSelectList(item.parentNode)
                    }
                    if (evt.composedPath().includes(this.rightItemWindow)) {
                        this.addItemToSelectList(item.parentNode)
                    }
                })
            })
    }

    populateMultiselect() {
        Object.values(this.originalSelect.options).forEach((selectOption) => {
            let option = document.createElement('div')
            option.innerHTML = `<div class="select-item--text">${selectOption.text}</div>
                                <div class="select-item--buttons">
                                    <div class="btn btn-default" data-method="up"><span class="up"></span></div>
                                    <div class="btn btn-default" data-method="down"><span class="down"></span></div>
                                </div>`
            option.dataset.value = selectOption.value
            option.dataset.selected = selectOption.selected ? '1' : '0'
            option.classList.add('select-item')
            this.rightItems.append(option)
        });
    }

    populateOriginalOptions() {
        const options = this.originalSelect.querySelectorAll(`option`)
        let originalOptions = [];
        Object.entries(options).forEach((option) => {
            originalOptions[`${option[1].value}`] = option
        })
        this.originalOptions = originalOptions
    }

    updateSelectedItems() {
        this.rightItems.querySelectorAll('.select-item').forEach((item) => {
            if (
                this.originalSelect
                    .querySelector(`option[value="${item.dataset.value}"]`)
                    .selected
            ) {
                item.dataset.selected = 'selected'
                this.addItemToSelectList(item)
            } else {
                item.dataset.selected = ''
            }
        })
    }
}

test = new Multiselect('#groups')
